package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

func loadFiles(root string, endPath string, cb func(string, os.FileInfo, []byte)) {
	if endPath == "" {
		endPath = "/"
	}
	filepath.Walk(root, func(p string, f os.FileInfo, err error) error {
		if err != nil {
			return nil
		}
		if f.IsDir() {
			return nil
		}
		if !f.Mode().IsRegular() {
			return nil
		}
		content, errFile := ioutil.ReadFile(p)
		if errFile != nil {
			return errFile
		}
		p = strings.Replace(p, root, "", 1)
		p = filepath.Join(endPath, p)
		cb(p, f, content)
		return nil
	})
}

var fileTemplate = `package {{.package}}

import (
	"bytes"
	"encoding/base64"
	"log"
	"net/http"
	"os"
	"time"
)

type virtualFile struct {
	name     string
	bytes    string
	size     int64
	mode     os.FileMode
	modeTime time.Time
}

// FileInfo virtual file informations.
type FileInfo struct {
	name     string
	size     int64
	mode     os.FileMode
	modeTime time.Time
}

// Name .
func (f FileInfo) Name() string {
	return f.name
}

// Size .
func (f FileInfo) Size() int64 {
	return f.size
}

// Mode .
func (f FileInfo) Mode() os.FileMode {
	return f.mode
}

// ModeTime .
func (f FileInfo) ModTime() time.Time {
	return f.modeTime
}

// IsDir .
func (f FileInfo) IsDir() bool {
	return false
}

// Sys .
func (f FileInfo) Sys() interface{} {
	return false
}

type File struct {
	*bytes.Reader
	info *FileInfo
}

func (f File) Close() error {
	return nil
}

func (f File) Readdir(count int) ([]os.FileInfo, error) {
	return []os.FileInfo{}, nil
}

func (f File) Stat() (os.FileInfo, error) {
	return f.info, nil
}

func Open(p string) (http.File, error) {
	f, ok := files[p]
	if !ok {
		return nil, os.ErrNotExist
	}
	b, err := base64.StdEncoding.DecodeString(f.bytes)
	if err != nil {
		log.Println(err)
	}
	vf := File{
		bytes.NewReader(b),
		&FileInfo{
			name:     f.name,
			size:     f.size,
			mode:     f.mode,
			modeTime: f.modeTime,
		},
	}
	return vf, nil
}

var files = map[string]virtualFile{}

type httpfs struct{}

func (h httpfs) Open(p string) (http.File, error) {
	return Open(p)
}

func NewHttpFS() http.FileSystem {
	return &httpfs{}
}

func init() {
`

func main() {
	var baseDir string
	var root string
	var packageName string
	flag.StringVar(&baseDir, "dir", "./", "directory")
	flag.StringVar(&root, "root", "/", "virtual file system root")
	flag.StringVar(&packageName, "package", "emb", "package name of the output file")
	flag.Parse()
	if packageName == "" {
		log.Fatalln("package name cannot be empty")
	}
	tpl, _ := template.New("").Parse(fileTemplate)
	tpl.Execute(os.Stdout, map[string]interface{}{
		"package": packageName,
	})
	loadFiles(baseDir, root, func(p string, f os.FileInfo, content []byte) {
		fmt.Printf(`	files["%s"] = virtualFile{"%s", "`, p, p)
		encoder := base64.NewEncoder(base64.StdEncoding, os.Stdout)
		encoder.Write(content)
		encoder.Close()
		fmt.Printf(`", %d, os.FileMode(%d), time.Unix(%d, 0)}`+"\n",
			f.Size(),
			f.Mode(),
			f.ModTime().Unix(),
		)
	})
	fmt.Println("}")
}
